<?php

use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     $nama = 'Griscipta';
//     return view('index', ['nama' => $nama]);
// });

// Route::get('/tentang', function () {
//     return view('tentang');
// });

Route::get('/', [PagesController::class, 'beranda']);
Route::get('/tentang', [PagesController::class, 'tentang']);
Route::get('/wisata', [PagesController::class, 'wisata']);
