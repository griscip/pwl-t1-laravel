@extends('layout.main')

@section('title', 'Wisata Kab Kediri')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="mt-5 mb-5">Daftar Wisata di Kabupaten Kediri</h1>

            <table class="table">
                <thead class="table-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Wisata</th>
                        <th scope="col">Lokasi Wisata</th>
                        <th scope="col">Harga Tiket</th>
                        <th scope="col">Link Google Maps</th>
                    <tr>
                </thead>
                @foreach($data as $wisata)
                <tbody>
                    <tr>
                        <th scope="row">{{$wisata[0]}}</th>
                        <td>{{$wisata[1]}}</td>
                        <td>{{$wisata[2]}}</td>
                        <td>{{$wisata[3]}}</td>
                        <td><a class="w3-button w3-margin-bottom btn btn-primary" href="{{$wisata[4]}}">Google Maps</a></td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection