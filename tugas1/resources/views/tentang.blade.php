@extends('layout.main')

@section('title', 'Tentang')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-5 mb-5">Tentang {{$nama}}</h1>
        </div>
    </div>
    <div class="accordion" id="accordionExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Sekilas Tentang Kabupaten Kediri
                </button>
            </h2>
            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <strong>Kabupaten Kediri adalah kabupaten di Provinsi Jawa Timur, Indonesia.</strong> </br> </br> Pusat pemerintahan berada di Kediri meskipun pemindahan pusat pemerintahan ke Pare telah lama direncanakan dan bahkan sekarang dibatalkan. Akhirnya pada saat ini ibu kota Kabupaten Kediri secara de jure berada di Kecamatan Ngasem. Kabupaten ini berbatasan dengan Kabupaten Jombang di utara, Kabupaten Malang di timur, Kabupaten Blitar dan Kabupaten Tulungagung di selatan, serta Kabupaten Nganjuk di barat dan utara. Kabupaten Kediri memiliki luas wilayah 1.386.05 km² dengan 26 kecamatan.
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Sejarah Singkat Kabupaten Kediri
                </button>
            </h2>
            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <strong>Menurut penelitian dari para ahli lembaga Javanologi, Drs. M.M. Soekarton Kartoadmodjo, Kediri lahir pada Maret 804 Masehi.</strong> </br> </br>Sekitar tahun itulah, Kediri mulai disebut-sebut sebagai nama tempat maupun negara. Belum ada sumber resmi seperti prasasti maupun dokumen tertulis lainnya yang dapat menyebutkan, kapan sebenarnya Kediri ini benar-benar menjadi pusat dari sebuah Pemerintahan maupun sebagai mana tempat.Dari prasasti yang diketemukan kala itu, masih belum ada pemisah wilayah administratif seperti sekarang ini. Yaitu adanya Kabupaten dan Kodya Kediri, sehingga peringatan Hari Jadi Kediri yang sekarang ini masih merupakan milik dua wilayah dengan dua kepala wilayah pula. Menurut para ahli, baik Kadiri maupun Kediri sama-sama berasal dari bahasa Sansekerta, dalam etimologi "Kadiri" disebut sebagai "Kedi" yang artinya "Mandul", tidak berdatang bulan (aprodit). Dalam bahasa Jawa Kuno, "Kedi" juga mempunyai arti "Dikebiri" atau dukun. </br> </br>
                    Menurut Drs. M.M. Soekarton Kartoadmodjo, nama Kediri tidak ada kaitannya dengan "Kedi" maupun tokok "Rara Kilisuci". Namun berasal dari kata "diri" yang berarti "adeg" (berdiri) yang mendapat awalan "Ka" yang dalam bahasa Jawa Kuno berarti "Menjadi Raja".Kediri juga dapat berarti mandiri atau berdiri tegak, berkepribadian atau berswasembada. Jadi pendapat yang mengkaitkan Kediri dengan perempuan, apalagi dengan Kedi kurang beralasan. </br></br>
                    Menurut Drs. Soepomo Poejo Soedarmo, dalam kamus Melayu, kata "Kediri" dan "Kendiri" sering menggantikan kata sendiri.Perubahan pengucapan "Kadiri" menjadi "Kediri" menurut Drs. Soepomo paling tidak ada dua gejala. Yang pertama, gejala usia tua dan gejala informalisasi. Hal ini berdasarkan pada kebiasaan dalam rumpun bahasa Austronesia sebelah barat, dimana perubahan seperti tadi sering terjadi.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection