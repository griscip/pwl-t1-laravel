@extends('layout.main')

@section('title', 'Wisata Kab Kediri')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-5 mb-5">Selamat Datang... </br> Di Kawasan Wisata Kabupaten Kediri</h1>
        </div>
    </div>
    <div class="w3-row-padding">

        <div class="w3-half w3-margin-bottom">
            <div class="w3-container w3-white">
                <h3 class="mt-3">Simpang Lima Gumul</h3>
                <p class="w3-opacity">Harga tiket : Gratis</p>
                <p>Monumen Simpang Lima Gumul (SLG) adalah bangunan yang menjadi ikon Kabupaten Kediri karena menyerupai Arc de Triomphe di Paris, Prancis</p>
                <p class="w3-opacity">Alamat: Tugurejo, Kec. Ngasem, Kediri, Jawa Timur 64182</p>
                <a class="w3-button w3-margin-bottom btn btn-primary" href="{{ url('https://goo.gl/maps/kxxfBW1u23rUSgSU6') }}">Google Maps</a>
            </div>
        </div>
        <div class="w3-half w3-margin-bottom">
            <div class="w3-container w3-white">
                <h3 class="mt-3">Wisata Kampung Indian Kediri</h3>
                <p class="w3-opacity">Harga tiket : Rp 10.000</p>
                <p>Wisata Kampung Indian Kediri merupakan wisata taman hiburan kecil bertema Amerika asli dengan aktor berkostum serta terdapat kolam renang.</p>
                <p class="w3-opacity">Alamat : Sempu, Ngancar, Kediri, Jawa Timur 64291</p>
                <a class="w3-button w3-margin-bottom btn btn-primary" href="{{ url('https://g.page/kampungindiankediri?share') }}">Google Maps</a>
            </div>
        </div>
    </div>
</div>
@endsection