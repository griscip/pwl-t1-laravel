<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function beranda()
    {
        return view('index');
    }
    public function tentang()
    {
        return view('tentang', ['nama' => 'Kabupaten Kediri']);
    }
    public function wisata()
    {
        $wisata = [
            ["1", "Simpang Lima Gumul", "Tugurejo, Kec. Ngasem", "Gratis", "https://goo.gl/maps/kxxfBW1u23rUSgSU6"],
            ["2", "Sumber Ubalan", "Sagi, Jarak, Kec. Plosoklaten", "Rp 5000", "https://goo.gl/maps/EKmGfQK2bGsHybCk7"],
            ["3", "Gumul Paradise Island", "Kompleks Simpang Lima Gumul, Sumberejo, Dadapan, Sumberejo, Kec. Ngasem", "Rp 35000", "https://goo.gl/maps/2U4LXV3GYDtqfoq77"],
            ["4", "Air Terjun Dolo", "Dusun Besuki, Desa Jugo, Kecamatan Mojo, Hutan, Blimbing", "Rp 7500", "https://goo.gl/maps/gzsDZ2gvp3hC2A3o9"],
            ["5", "Bendungan Gerak Watu Wuri", "Gampeng, Jabon, Kec. Gampengrejo", "Rp 11000", "https://goo.gl/maps/Qwcs3ZzDQ3aHFXUY9"],
            ["6", "Candi Tegowangi", "Candirejo, Tegowangi, Kec. Plemahan", "Gratis", "https://goo.gl/maps/pyH5XUN4N13KuEm59"],
            ["7", "Candi Surowono", "Surowono, Canggu, Kec. Badas", "Gratis", "https://goo.gl/maps/t4GPXwFNk4YUmfTJ6"],
            ["8", "Gua Maria Lourdes Puh Sarang", "Sukorame, Puhsarang, Kec. Semen", "Gratis", "https://goo.gl/maps/XxNTAgTaZ8eDmaWA6"],
            ["9", "Pura Sri Aji Jayabaya", "Gropyok, Tanon, Kec. Papar", "Gratis", "https://goo.gl/maps/m3a3GYEMMdc2eiek6"],
            ["10", "Kampoeng Anggrek Kediri", "Ringinsari, Sempu, Ngancar", "Rp 12000", "https://goo.gl/maps/iZN6fbN5FzfN6N866"]
        ];
        return view('wisata', ['data' => $wisata]);
    }
}
